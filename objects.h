#ifndef GAME_OBJECTS_H
#define GAME_OBJECTS_H

#include "header.h"
#include "objectmanager.h"

//=========================================================
// BUTTON
//=========================================================
class Button : public Object
{
public:
	Sprite *sprite;
	Object *o;
	MESSAGE message;
	bool use;

	Button(V2 pos);

	virtual void Update(float dt){};
	virtual void Input(Object *object, MESSAGE type, void* data, float dt);
	virtual void Clear();
	virtual void Reset();
};

//=========================================================
// SWITCH
//=========================================================
class Switch : public Button
{
public:
	Switch(V2 pos);

	void Update(float dt);
	void Input(Object *object, MESSAGE type, void* data, float dt);
	void Clear();
};

//=========================================================
// PACKAGE
//=========================================================
class Package : public Object
{
protected:
	float capacity;
	float charged;

public:
	Package():Object()
	{
		type = PACKAGE;
		capacity = 1.0f;
		charged = 0.0f;
	}
};

//=========================================================
// PUSZKA
//=========================================================
class Can : public Package
{
protected:

public:
	b2Body *body;
	Sprite *sprite;
	Sprite *sprite2;

	float painting;
	float maxPainting;

	Can(V2 pos);
	virtual void Update(float dt);
	virtual void Input(Object *object, MESSAGE type, void* data, float dt);
	virtual void Clear();
	virtual void Reset(){};
};


//=========================================================
// RESPAWN PUSZEK
//=========================================================
class CanCreator : public Object
{
public:
	float speed;
	float prevCreate;
	Sprite *sprite;
	Sprite *sprite2;

	CanCreator(V2 pos);
	virtual void Update(float dt){prevCreate -= dt;};
	virtual void Input(Object *object, MESSAGE type, void* data, float dt);
	virtual void Clear();
	virtual void Reset(){};
};


//=========================================================
// LICZNIK
//=========================================================
class Timer : public Object
{
protected:
	float time;
	bool play;

public:
	Object* o;
	float delay;
	bool stopAndPlay;
	MESSAGE message;

	Timer();
	virtual void Update(float dt);
	virtual void Input(Object *object, MESSAGE type, void* data, float dt);
	virtual void Clear(){};
	virtual void Reset(){};
};


//=========================================================
// Licznik z progressbarem
//=========================================================
class VisualTimer : public Timer
{

public:
	Progressbar *pb;

	VisualTimer(V2 pos);
	virtual void Update(float dt);
	virtual void Input(Object *object, MESSAGE type, void* data, float dt)
					  {Timer::Input(object, type, data, dt);};
	virtual void Clear();
	virtual void Reset(){};
};

//=========================================================
// CZUJNIK
//=========================================================
class Sensor : public Object
{
public:
	Object *o;
	b2Body *body;
	Sprite *sprite;
	MESSAGE message;

	Sensor(V2 pos);
	virtual void Update(float dt);
	virtual void Input(Object *object, MESSAGE type, void* data, float dt);
	virtual void Clear();
	virtual void Reset(){};
};

//=========================================================
// Tasmociag
//=========================================================
class Conveyor : public Object
{
public:
	b2Body *body;
	Sprite *sleft, *sright, **slink, **slinkrev;
	int size;
	bool run;

	Conveyor(V2 pos, UINT size);
	virtual void Update(float dt);
	virtual void Input(Object *object, MESSAGE type, void* data, float dt);
	virtual void Clear();
	virtual void Reset();
};


//=========================================================
// Malarnia
//=========================================================
class Paint : public Object
{
public:
	b2Body *body;
	Sprite *sprite;
	Particles *p;
	Progressbar *pb;
	float farba;

	Paint(V2 pos);
	virtual void Update(float dt);
	virtual void Input(Object *object, MESSAGE type, void* data, float dt);
	virtual void Clear();
	virtual void Reset(){p->StopRespawn();};
};

//=========================================================
// Pudelko do pakowania
//=========================================================
class FinishBox : public Object
{
public:
	b2Body *body;
	Sprite *sprite;

	UINT sk;
	UINT skm;
	UINT sknm;

	FinishBox(V2 pos);
	virtual void Update(float dt);
	virtual void Input(Object *object, MESSAGE type, void* data, float dt){};
	virtual void Clear();
	virtual void Reset(){};
};
/*
class Sensor : public Object
{
public:
	b2Body *body;
	Sprite *sprite;
	Particles *p;

	Sensor(V2 pos);
	virtual void Update(float dt);
	virtual void Input(Object *object, int type, float dt) {};
};

class Conveyor : public Object
{
public:
	b2Body *roll[2];
	b2Body *tape[10];
	Sprite *sRoll[2];
	Sprite *sTape[10];

	UINT itape;

	Conveyor(V2 pos);
	virtual void Update(float dt);
	virtual void Input(Object *object, int type, float dt){};
};*/

#endif