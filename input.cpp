#ifndef CORE_INPUT_CPP
#define CORE_INPUT_CPP

#include "input.h"

//=========================================================
// INPUT SINGLETON
//=========================================================
Input& Input::GetInstance()
{
	static Input instance;
	return instance;
}

//=========================================================
// INPUT INIT
//=========================================================
bool Input::Init()
{
	keyState = SDL_GetKeyState(NULL);
	KeySnapshot();
	return true;
}

//=========================================================
// INPUT UPDATEos
//=========================================================
bool Input::Update()
{
	KeySnapshot();
	MouseSnapshot();
	while(SDL_PollEvent(&event))
	{
		if( event.type == SDL_QUIT )
		{
			return false;
		}
	}

	return true;
}

//=========================================================
// INPUT KEY PRESSED
//=========================================================
bool Input::KeyPressed(SDLKey key)
{
	return keyState[key] ? true : false;
}

//=========================================================
// INPUT KEY TAPPED
//=========================================================
bool Input::KeyTapped(SDLKey key)
{
	return keyState[key] && !keyStatePrev[key] ? true : false;
}

//=========================================================
// INPUT KEY SNAPSHOT
//=========================================================
void Input::KeySnapshot()
{
	for(int i=0; i<323; i++)
		keyStatePrev[i] = keyState[i];
}

//=========================================================
// INPUT MOUSE SNAPSHOT
//=========================================================
void Input::MouseSnapshot()
{
	mousePrevState = SDL_GetMouseState(NULL, NULL);
}

//=========================================================
// INPUT GET MOUSE POSITION
//=========================================================
V2 Input::GetMousePosition()
{
	int pos[2];
	V2 ret;
	SDL_GetMouseState(&pos[0], &pos[1]);
	ret.x = (float)pos[0];
	ret.y = (float)pos[1];
	return ret;
}

//=========================================================
// INPUT MOUSE BUTTON PRESSED
//=========================================================
bool Input::MousePressed(char button)
{
	return SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(button) ? true : false;
}

//=========================================================
// INPUT MOUSE BUTTON TAPPED
//=========================================================
bool Input::MouseTapped(char button)
{
	return SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(button) && !(mousePrevState & SDL_BUTTON(button)) ? true : false;
}

#endif